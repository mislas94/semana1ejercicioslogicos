const capitalizeLetters = (str) => {
  //Implementación
  let words = str.split(' ');
  let newPhrase = [];

  words.forEach((word) => {
    const upper = word.charAt(0).toUpperCase();
    const remain = word.slice(1);
    const newWord = upper + remain;
    newPhrase.push(newWord);
  });
  return newPhrase.join(' ');
};

module.exports = {
  capitalizeLetters,
};
